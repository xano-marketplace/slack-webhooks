import {Injectable} from '@angular/core';
import {ConfigService} from "../_demo-core/config.service";
import {ApiService} from "../_demo-core/api.service";
import {Observable} from "rxjs";

@Injectable({
	providedIn: 'root'
})
export class SlackService {

	constructor(
		private configService: ConfigService,
		private apiService: ApiService
	) {
	}

	public slackWebhook(text): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/slack`,
			params: { text }
		});
	}
}

import {Component, OnInit} from '@angular/core';
import {ConfigService, XanoConfig} from '../_demo-core/config.service';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {
	constructor(
		private configService: ConfigService,
	) {
	}

	public apiConfigured: boolean = false;
	public config: XanoConfig;

	ngOnInit(): void {
		this.config = this.configService.config;
		this.configService.isConfigured().subscribe(apiUrl => {
			this.apiConfigured = !!apiUrl;
		});
	}
}

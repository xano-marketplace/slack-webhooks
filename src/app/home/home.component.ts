import {Component, OnInit} from '@angular/core';
import {ConfigService, XanoConfig} from "../_demo-core/config.service";
import {SlackService} from "../api-services/slack.service";
import {FormControl, Validators} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	public config: XanoConfig;
	public configured: boolean = false;
	public response;
	public textInput: FormControl = new FormControl('', Validators.required)

	constructor(
		private configService: ConfigService,
		private slackService: SlackService,
		private snackbar: MatSnackBar
	) {
	}

	ngOnInit(): void {
		this.config = this.configService.config;
		this.configService.xanoApiUrl.subscribe(apiUrl => this.configured = !!apiUrl)
	}

	public send(): void {
		if (this.textInput.valid) {
			this.slackService.slackWebhook(this.textInput.value).subscribe(res => {
				if(res) {
					this.textInput.reset()
					this.snackbar.open('Message successfully sent. Check your Slack channel!', 'Success')
				}
			}, error => this.configService.showErrorSnack(error))
		}
	}
}

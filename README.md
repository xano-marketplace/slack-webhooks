# Xano Slack Webhooks

This simple demo demonstrates Slack webhook integration in your Xano backend.


### Setup & Dependencies

#### List of Dependencies
* material
* bootstrap css
* lodash-es

#### Setup

You can use either npm or yarn to install dependencies.

run `npm install` or `yarn`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
